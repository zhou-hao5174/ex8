package com.example.ex8.weather

data class CityInfo(
    val city: String,
    val citykey: String,
    val parent: String,
    val updateTime: String
)
package com.example.ex8.weather

data class Data(
    val forecast: List<Forecast>,
    val ganmao: String,
    val pm10: Int,
    val pm25: Int,
    val quality: String,
    val shidu: String,
    val wendu: String,
    val yesterday: Yesterday
)